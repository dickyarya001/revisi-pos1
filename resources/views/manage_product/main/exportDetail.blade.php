<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Barang</title>

    <style>
        #myTable thead, #myTable tbody,  #myTable tr,  #myTable th, #myTable td{
            border: 1px solid black;
        }

        #myTable2 thead, #myTable tbody,  #myTable2 tr,  #myTable th, #myTable2 td{
            border: 1px solid black;
        }
    </style>
</head>

<body style="background-color:white">
    <p style="font-weight:bold; font-size: 20px; text-align:left;">{{ $product->product_type->nama_produk }}</p>
    <h5 style="text-align:left;font-weight:bold;">Daftar Barang Pusat - Keluar - Januari 2022</h5>
    <table id="myTable"
        class="table table-hover table-striped table-light display sortable text-left text-nowrap"
        cellspacing="0">
        <thead>
            <tr id="_judul" onkeyup="_filter()" id="myFilter">
                @if(auth()->user()->user_position == "reseller")
                    <th>Tanggal Keluar</th>
                    <th>No Transaksi</th>
                    <th>Jumlah barang Keluar</th>
                @else
                    <th>Tanggal Keluar</th>
                    <th>Admin</th>
                    <th>No Transaksi</th>
                    <th>Jumlah barang Keluar</th>
                @endif
            </tr>
        </thead>

        <tbody>
            @php
                $totalKeluar=0;
            @endphp
            @foreach($keluar as $k)
                <tr>
                    @if(auth()->user()->user_position == "reseller")
                        <td>{{ $k->created_at->format('d/m/y H:i:s') }}</td>
                        <td>{{ $k->nama_toko }}</td>
                        <td>{{ number_format($k->jumlah_produk, 0, ',', '.') }} pcs</td>
                        @php
                            $totalKeluar+=$k->jumlah_produk;
                        @endphp
                    @else
                        <td>{{ $k->updated_at->format('d/m/y H:i:s') }}</td>
                        <td>{{ $k->nama_approve }}</td>
                        <td>{{ $k->transaction_code }}</td>
                        <td>{{ number_format($k->jumlah, 0, ',', '.') }} pcs</td>
                        @php
                            $totalKeluar+=$k->jumlah;
                        @endphp
                    @endif
                </tr>
            @endforeach
            @if(auth()->user()->id_group != 1)
                @foreach($keluarKasir as $k)
                    <tr>
                        @if(auth()->user()->user_position == "reseller")
                            <td>{{ $k->updated_at->format('d/m/y H:i:s') }}</td>
                            <td>{{ $k->transaction_code }}</td>
                            <td>{{ number_format($k->jumlah, 0, ',', '.') }} pcs</td>
                        @else
                            <td>{{ $k->updated_at->format('d/m/y H:i:s') }}</td>
                            <td>{{ $k->nama_input }}</td>
                            <td>{{ $k->transaction_code }}</td>
                            <td>{{ number_format($k->jumlah, 0, ',', '.') }} pcs</td>
                        @endif
                    </tr>
                    @php
                        $totalKeluar+=$k->jumlah;
                    @endphp
                @endforeach
            @endif
            @foreach($keluarRetur as $k)
                <tr>
                    <td>{{ $k->updated_at->format('d/m/y H:i:s') }}</td>
                    <td>{{ $k->nama_input }}</td>
                    <td>{{ $k->surat_keluar }}</td>
                    <td>{{ number_format($k->jumlah, 0, ',', '.') }} pcs</td>
                </tr>
                @php
                    $totalKeluar+=$k->jumlah;
                @endphp
            @endforeach
        </tbody>
    </table>
    <p style="font-weight:bold">Jumlah Barang Keluar : {{ number_format($totalKeluar, 0, ',', '.') }} pcs</p>

    <h5 style="text-align:left;font-weight:bold;">Daftar Barang Pusat - Masuk - Januari 2022</h5>
    <table class="table table-hover table-striped table-light display sortable text-nowrap text-left"
        cellspacing="0" id="myTable2">
        <thead>
            <tr id="_judul" onkeyup="_filter()" id="myFilter">
                @if(auth()->user()->id_group == 1)
                <th>Tanggal Masuk</th>
                <th>Admin</th>
                <th>Kode Pasok</th>
                <th>Jumlah barang Masuk</th>
                @elseif(auth()->user()->user_position == "reseller")
                <th>Tanggal Masuk</th>
                <th>No. Transaksi</th>
                <th>Jumlah barang Masuk</th>
                @else
                <th>Tanggal Masuk</th>
                <th>Admin</th>
                <th>No. Transaksi</th>
                <th>Jumlah barang Masuk</th>
                @endif
            </tr>
        </thead>

        <tbody>
            @php
                $totalMasuk=0;
            @endphp
            @foreach($masuk as $m)
            <tr>
                @if(auth()->user()->id_group == 1)
                <td>{{ $m->updated_at->format('d/m/y H:i:s') }}</td>
                <td>{{ $m->nama_input }}</td>
                <td>{{ $m->kode_pasok }}</td>
                <td>{{ number_format($m->jumlah, 0, ',', '.') }} pcs</td>
                @elseif(auth()->user()->user_position == "reseller")
                <td>{{ $m->updated_at->format('d/m/y H:i:s') }}</td>
                <td>{{ $m->transaction_code }}</td>
                <td>{{ number_format($m->jumlah, 0, ',', '.') }} pcs</td>
                @else
                <td>{{ $m->updated_at->format('d/m/y H:i:s') }}</td>
                <td>{{ $m->nama_input }}</td>
                <td>{{ $m->transaction_code }}</td>
                <td>{{ number_format($m->jumlah, 0, ',', '.') }} pcs</td>
                @endif
            </tr>
            @php
                $totalMasuk+=$m->jumlah;
            @endphp
            @endforeach
        </tbody>
    </table>
    <p style="font-weight:bold">Jumlah Barang Masuk : {{ number_format($totalMasuk, 0, ',', '.') }} pcs</p>

    @canany(['superadmin_pabrik','superadmin_distributor'])
    <h5 style="text-align:left;font-weight:bold;">Daftar Barang Pusat - Hilang - Januari 2022</h5>
    <table class="table table-hover table-striped table-light display sortable text-nowrap text-left"
        cellspacing="0" id="myTable2">
        <thead>
            <tr id="_judul" onkeyup="_filter()" id="myFilter">
                <th>Tanggal Barang Hilang</th>
                <th>Admin</th>
                <th>Jumlah Barang Hilang</th>
            </tr>
        </thead>

        <tbody>
            @php
                $totalHilang=0;
            @endphp
            @foreach($hilang as $h)
            <tr>
                <td>{{ $h->updated_at->format('d/m/y H:i:s') }}</td>
                <td>{{ $h->nama_input }}</td>
                <td>{{ number_format($h->stok_hilang, 0, ',', '.') }} pcs</td>
            </tr>
            @php
                $totalHilang+=$h->stok_hilang;
            @endphp
            @endforeach
        </tbody>
    </table>
    <p style="font-weight:bold">Jumlah Barang Hilang : {{ number_format($totalHilang, 0, ',', '.') }} pcs</p>
    @endcan
</body>
</html>