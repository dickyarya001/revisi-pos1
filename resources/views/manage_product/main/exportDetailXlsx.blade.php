<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Barang</title>
    <style>
        #myTable thead, #myTable tbody,  #myTable tr,  #myTable th, #myTable td{
            border: 1px solid black;
        }

        #myTable2 thead, #myTable tbody,  #myTable2 tr,  #myTable th, #myTable2 td{
            border: 1px solid black;
        }
    </style>
</head>

<body style="background-color:white">
    <table>
        <tr>
            <td>{{ $product->product_type->nama_produk }}</td>
        </tr>
        <tr>
            <td>Daftar Barang Pusat - Keluar - Januari 2022</td>
        </tr>
    </table>
    <br>
    <table>
        <thead>
            <tr>
                @if(auth()->user()->user_position == "reseller")
                    <th>Tanggal Keluar</th>
                    <th>No Transaksi</th>
                    <th>Jumlah barang Keluar</th>
                @else
                    <th>Tanggal Keluar</th>
                    <th>Admin</th>
                    <th>No Transaksi</th>
                    <th>Jumlah barang Keluar</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @php
                $totalKeluar=0;
            @endphp
            @foreach($keluar as $k)
                <tr>
                    @if(auth()->user()->user_position == "reseller")
                        <td>{{ $k->created_at->format('d/m/y H:i:s') }}</td>
                        <td>{{ $k->nama_toko }}</td>
                        <td>{{ number_format($k->jumlah_produk, 0, ',', '.') }} pcs</td>
                        @php
                            $totalKeluar+=$k->jumlah_produk;
                        @endphp
                    @else
                        <td>{{ $k->updated_at->format('d/m/y H:i:s') }}</td>
                        <td>{{ $k->nama_approve }}</td>
                        <td>{{ $k->transaction_code }}</td>
                        <td>{{ number_format($k->jumlah, 0, ',', '.') }} pcs</td>
                        @php
                            $totalKeluar+=$k->jumlah;
                        @endphp
                    @endif
                </tr>
            @endforeach
            @if(auth()->user()->id_group != 1)
                @foreach($keluarKasir as $k)
                    <tr>
                        @if(auth()->user()->user_position == "reseller")
                            <td>{{ $k->updated_at->format('d/m/y H:i:s') }}</td>
                            <td>{{ $k->transaction_code }}</td>
                            <td>{{ number_format($k->jumlah, 0, ',', '.') }} pcs</td>
                        @else
                            <td>{{ $k->updated_at->format('d/m/y H:i:s') }}</td>
                            <td>{{ $k->nama_input }}</td>
                            <td>{{ $k->transaction_code }}</td>
                            <td>{{ number_format($k->jumlah, 0, ',', '.') }} pcs</td>
                        @endif
                    </tr>
                    @php
                        $totalKeluar+=$k->jumlah;
                    @endphp
                @endforeach
            @endif
            @foreach($keluarRetur as $k)
                <tr>
                    <td>{{ $k->updated_at->format('d/m/y H:i:s') }}</td>
                    <td>{{ $k->nama_input }}</td>
                    <td>{{ $k->surat_keluar }}</td>
                    <td>{{ number_format($k->jumlah, 0, ',', '.') }} pcs</td>
                </tr>
                @php
                    $totalKeluar+=$k->jumlah;
                @endphp
            @endforeach
        </tbody>
    </table>
    <br>
    <table>
        <tr>
            <td>Jumlah Barang Keluar : {{ number_format($totalKeluar, 0, ',', '.') }} pcs</td>
        </tr>
    </table>
    <br><br>
    <table>
        <tr>
            <td>Daftar Barang Pusat - Masuk - Januari 2022</td>
        </tr>
    </table>
    <br>
    <table>
        <thead>
            <tr>
                @if(auth()->user()->id_group == 1)
                <th>Tanggal Masuk</th>
                <th>Admin</th>
                <th>Kode Pasok</th>
                <th>Jumlah barang Masuk</th>
                @elseif(auth()->user()->user_position == "reseller")
                <th>Tanggal Masuk</th>
                <th>No. Transaksi</th>
                <th>Jumlah barang Masuk</th>
                @else
                <th>Tanggal Masuk</th>
                <th>Admin</th>
                <th>No. Transaksi</th>
                <th>Jumlah barang Masuk</th>
                @endif
            </tr>
        </thead>

        <tbody>
            @php
                $totalMasuk=0;
            @endphp
            @foreach($masuk as $m)
            <tr>
                @if(auth()->user()->id_group == 1)
                <td>{{ $m->updated_at->format('d/m/y H:i:s') }}</td>
                <td>{{ $m->nama_input }}</td>
                <td>{{ $m->kode_pasok }}</td>
                <td>{{ number_format($m->jumlah, 0, ',', '.') }} pcs</td>
                @elseif(auth()->user()->user_position == "reseller")
                <td>{{ $m->updated_at->format('d/m/y H:i:s') }}</td>
                <td>{{ $m->transaction_code }}</td>
                <td>{{ number_format($m->jumlah, 0, ',', '.') }} pcs</td>
                @else
                <td>{{ $m->updated_at->format('d/m/y H:i:s') }}</td>
                <td>{{ $m->nama_input }}</td>
                <td>{{ $m->transaction_code }}</td>
                <td>{{ number_format($m->jumlah, 0, ',', '.') }} pcs</td>
                @endif
            </tr>
            @php
                $totalMasuk+=$m->jumlah;
            @endphp
            @endforeach
        </tbody>
    </table>
    <table>
        <tr>
            <td>Jumlah Barang Masuk : {{ number_format($totalMasuk, 0, ',', '.') }} pcs</td>
        </tr>
    </table>
</body>
</html>