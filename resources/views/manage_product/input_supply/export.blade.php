<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ASTANA</title>
    <style>
        #myTable thead, #myTable tbody,  #myTable tr,  #myTable th, #myTable td{
            border: 1px solid black;
        }
    </style>
</head>
<body style="background-color:white">
    <p style="font-weight:bold; font-size: 20px; text-align:left;">Input Pasok Barang</p>
    <table cellspacing="0" id="myTable">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Kode Pasok</th>
                <th>Nama Pasok</th>
                <th>Total Pasok</th>
                <th>Admin</th>
            </tr>
        </thead>
        <tbody>
            @php
                $totalPembelian = 0;
            @endphp
            @foreach($histories as $history)
                <tr>
                    <td>{{ $history->created_at->format('j F Y H:m:s') }}</td>
                    <td>{{ $history->kode_pasok }}</td>
                    <td>{{ $history->nama_supplier }}</td>
                    <td>Rp {{ number_format($history->total, 0, ',', '.') }}</td>
                    <td>
                        {{ $history->nama_input }}
                    </td>
                </tr>
                @php
                $totalPembelian += $history->total;
            @endphp
            @endforeach
        </tbody>
    </table>
    <br>
    <table>
        <tr>
            <td>Total Pembelian Pasok Barang</td>
            <td>:</td>
            <td>Rp. {{ number_format($totalPembelian, 0, ',', '.') }}</td>
        </tr>
    </table>
</body>
</html>