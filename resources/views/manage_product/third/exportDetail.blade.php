<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Barang</title>

    <style>
        #myTable thead, #myTable tbody,  #myTable tr,  #myTable th, #myTable td{
            border: 1px solid black;
        }
    </style>
</head>

<body style="background-color:white">
    <p style="font-weight:bold; font-size: 20px; text-align:left;">
        Daftar Reseller {{ $distributor->firstname }} {{ $distributor->lastname }}
    </p>
    <table
        class="table table-hover table-striped table-light display sortable text-nowrap"
        cellspacing="0" id="myTable">
        <thead>
            <tr id="_judul" onkeyup="_filter()" id="myFilter">
                <th>ID</th>
                <th>Nama Reseller</th>
                <th>Alamat</th>
                <th>Kota</th>
                <th>Waktu Join</th>
                <th>Stock Produk</th>
            </tr>
        </thead>

        <tbody>
            @php
                $allStok=0;
            @endphp
            @foreach($resellers as $reseller)
                <tr>
                    <td>{{ $reseller->id }}</td>
                    <td>{{ $reseller->firstname }} {{ $reseller->lastname }}</td>
                    <td>{{ $reseller->address }}</td>
                    <td>{{ $reseller->city->name }}</td>
                    <td>{{ $reseller->created_at->format('j F Y') }}</td>
                    <td>{{ number_format($reseller->stock, 0, ',', '.') }} pcs</td>
                </tr>
                @php
                    $allStok+=$reseller->stock;
                @endphp
            @endforeach
        </tbody>
    </table>
    <table>
        <tr>
            <td>Total Reseller</td>
            <td>:</td>
            <td>{{ $resellers->count() }} Reseller</td>
        </tr>
        <tr>
            <td>Total Stock Produk Seluruh Reseller</td>
            <td>:</td>
            <td>{{ number_format($allStok, 0, ',', '.') }} pcs</td>
        </tr>
    </table>
</body>
</html>