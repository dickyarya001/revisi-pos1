<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Barang</title>

    <style>
        #myTable thead,
        #myTable tbody,
        #myTable tr,
        #myTable th,
        #myTable td {
            border: 1px solid black;
        }
    </style>
</head>

<body style="background-color:white">
    <p style="font-weight:bold; font-size: 20px; text-align:left;">
        @if (auth()->user()->id_group == 1)
            Daftar Barang Distributor
        @else
            Daftar Barang Reseller
        @endif
        - {{ $owner->firstname }} {{ $owner->lastname }}
    </p>

    <table>
        <tr>
            <td>Total Stock Parfum</td>
            <td>:</td>
            <td>{{ number_format($totalStok, 0, ',', '.') }} pcs</td>
        </tr>
        <tr>
            <td>Total Nilai Stok</td>
            <td>:</td>
            <td>{{ number_format($totalNilaiStok, 0, ',', '.') }} pcs</td>
        </tr>
    </table>

    <table class="table table-hover table-striped table-light display sortable text-nowrap" cellspacing="0"
        id="myTable">
        <thead>
            <tr id="_judul" onkeyup="_filter()" id="myFilter">
                <th>ID</th>
                <th>Barang</th>
                <th>Kategori</th>
                <th>Lokasi</th>
                <th>Stok</th>
                <th>Harga Jual</th>
                <th>Harga Modal</th>
                <th>Nilai Total</th>
                <th>Keterangan</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($products as $product)
                <tr>
                    <td>{{ $product->product_type->kode_produk }}</td>
                    <td>{{ $product->product_type->nama_produk }}</td>
                    <td>{{ $product->category->nama_kategori }}</td>
                    <td>{{ $product->product_type->lokasi_barang }}</td>
                    <td>{{ number_format($product->stok, 0, ',', '.') }} pcs</td>
                    <td>Rp {{ number_format($product->harga_jual, 0, ',', '.') }}</td>
                    <td>Rp {{ number_format($product->harga_modal, 0, ',', '.') }}</td>
                    <td>Rp {{ number_format($product->stok * $product->harga_modal, 0, ',', '.') }}</td>
                    <td>{{ $product->keterangan }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
