@extends('templates/print')

@section('css')

@endsection

@section('content')
<div class="wrapper">
    <div class="container ">
        <div class="row">
            <div class="col-sm-5">
                <img src="{{ asset('images/astanakop.jpeg') }}" style="width:25%">
            </div>
            <div class="col-sm-7">
                <div class="d-flex justify-content-end" style="font-weight:bold">
                    PT ASTANA CANTIKA INDONESIA
                </div>
                <div class="d-flex justify-content-end">
                    Pergudangan Sinar Buduran 3 Blok F8
                </div>
                <div class="d-flex justify-content-end">
                    Jl. Lingkar Timur Ds. Siwalanpanji Kec. Buduran Kab. Sidoarjo- Jawa Timur
                </div>
                <div class="d-flex justify-content-end">
                    Email: andaracantikaindonesia@gmail.com
                </div>
            </div>
        </div>
        <hr style="border-color:black;">
        <div class="row d-flex justify-content-center" style="font-weight:bold; font-size:24px">
        History Bonus - {{ $sales->firstname }} {{ $sales->lastname }}
        </div>
        <hr style="border-color:black;">
        <div class="row d-flex justify-content-center align-items-center">
            <div class="col-12 grid-margin ">
            <div class="iq-card">
                <div class="iq-card-body ">
                    <hr>
                    <table id="myTable" class="table text-left table-hover table-striped table-light"
                        id="myTable">
                        <thead>
                            <tr>
                                <th>Tanggal Bonus Masuk</th>
                                <th>Jumlah Bonus Masuk</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($histories as $history)
                            <tr>
                                <td>{{ $history->updated_at->format('d/m/y H:i:s') }}</td>
                                <td>{{ number_format($history->total_bonus_penjualan_sales, 0, ',', '.') }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>

<script>
@endsection