<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Detail Retur</title>

    <style>
        #myTable thead, #myTable tbody,  #myTable tr,  #myTable th, #myTable td{
            border: 1px solid black;
        }
    </style>
</head>

<body style="background-color:white">
    <br>
    <table>
        <tr>
            <td>
                @can('superadmin_pabrik')
                Retur Pabrik ke Supplier
                @endcan
                @can('superadmin_distributor')
                Retur Distributor ke Pabrik
                @endcan
                @can('reseller')
                Retur Reseller ke Distributor
                @endcan
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>No Nota</td>
            <td>:</td>
            <td>
                @if(auth()->user()->user_position == "superadmin_pabrik")
                {{ $retur->pasok->kode_pasok }}
                @else
                {{ $retur->transaction->transaction_code }}
                @endif
            </td>
        </tr>
        <tr>
            <td>Supplier</td>
            <td>:</td>
            <td>
                @if($retur->id_supplier == 0)
                Pabrik Astana
                @else
                {{ $retur->supplier->firstname }} {{ $retur->supplier->lastname }} 
                @endif
            </td>
        </tr>
        <tr>
            <td>No Surat Keluar</td>
            <td>:</td>
            <td>{{ $retur->surat_keluar }}</td>
        </tr>
        <tr>
            <td>Tanggal Retur</td>
            <td>:</td>
            <td>{{ $retur->updated_at->format('d/m/y H:i:s') }}</td>
        </tr>
        <tr>
            <td>Keterangan</td>
            <td>:</td>
            <td>{{ $retur->keterangan }}</td>
        </tr>
    </table>
    <br>
    <table id="myTable">
        <thead>
            <tr>
                <th scope="col">Nama Barang</th>
                <th scope="col">Kuantitas Barang yang Diretur</th>
                <th scope="col">Harga Satuan</th>
                <th scope="col">Harga Total</th>
            </tr>
        </thead>
        <tbody>
            @foreach($details as $detail)
                <tr>
                    <td scope="col">{{ $detail->nama_produk }}</td>
                    <td scope="col">{{ number_format($detail->jumlah, 0, ',', '.') }}</td>
                    <td scope="col">Rp. {{ number_format($detail->harga, 0, ',', '.') }}</td>
                    <td scope="col">Rp. {{ number_format($detail->jumlah*$detail->harga, 0, ',', '.') }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <table>
        <tr>
            <td>Total Retur</td>
            <td>:</td>
            <td>Rp. {{ number_format($retur->total, 0, ',', '.') }}</td>
        </tr>
    </table>
    </body>
</html>